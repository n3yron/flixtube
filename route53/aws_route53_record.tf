resource "aws_route53_record" "www" {
  zone_id = "Z36VNL1RN3KWDL"
  name    = var.dns_name
  type    = "A"
  ttl     = 60
  records = [var.myrecord]
}

# MYIP="$(kubectl get svc ingress-nginx-controller | grep -v NAME | awk '{print $4}')"
# terraform apply --auto-approve -var="myrecord=3.3.3.3"
# terraform apply --auto-approve -var="myrecord=$MYIP"