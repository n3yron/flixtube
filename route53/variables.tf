variable "myrecord" {
  default = "5.5.5.5"
}

variable "dns_name" {
  default = "n3yron.nebula.video"
}