# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/container_registry
resource "azurerm_container_registry" "acr" {
  name                = "n3yron"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku                 = "Basic"
  admin_enabled       = true
}

output "registry_hostname" {
  value = azurerm_container_registry.acr.login_server
}

output "registry_un" {
  value = azurerm_container_registry.acr.admin_username
}

output "registry_pw" {
  value     = azurerm_container_registry.acr.admin_password
  sensitive = true
}
