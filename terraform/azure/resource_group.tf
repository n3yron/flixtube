provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "${var.rg}-rg"
  location = "West US 2"
  #  location = "West US 2"

  tags = {
    environment = "demo"
    owner       = "oleksandr_dovnich_sre"
    dept        = "4566"
  }
}
