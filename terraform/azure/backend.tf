# terraform init -backend-config=backend.tfvars
# terraform init -backend-config="key=Sk0Sbw63l37izfWKszLjNK9jrV7tp9g4VisR+hR9m78Lt2bmq5bDTVT/TrZbNT1bq0QW0J3wnbnW+AStQNToeA=="
terraform {
  backend "azurerm" {
    resource_group_name  = "n3yron-terraform-state"
    storage_account_name = "n3yronterraformstate"
    container_name       = "vhds"
    key                  = "Sk0Sbw63l37izfWKszLjNK9jrV7tp9g4VisR+hR9m78Lt2bmq5bDTVT/TrZbNT1bq0QW0J3wnbnW+AStQNToeA=="
  }
}
