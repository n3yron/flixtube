resource "azurerm_kubernetes_cluster" "aks" {
  name                = "${var.rg}-aks"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  dns_prefix          = "${var.rg}-aks"

  default_node_pool {
    name       = "default"
    node_count = 4
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    environment = "demo"
    owner       = "oleksandr_dovnich_sre"
    dept        = "4566"
  }
}

output "client_certificate" {
  value     = azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate
  sensitive = true
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.aks.kube_config_raw

  sensitive = true
}
