
resource "azurerm_storage_account" "storage_account" {
  name                     = "${var.rg}storage"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "staging"
  }
}

resource "azurerm_storage_container" "container" {
  name                  = random_id.random.hex
  storage_account_name  = azurerm_storage_account.storage_account.name
  container_access_type = "private"
}


resource "random_id" "random" {
  byte_length = 12
}
