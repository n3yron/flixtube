# RT for public subnet
resource "aws_route_table" "gw" {
  vpc_id = aws_vpc.main-vpc.id
  count  = var.rt ? 1 : 0

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name  = var.vpc_name
    Owner = var.owner
  }
}

# RT for NAT1
resource "aws_route_table" "private_1" {
  vpc_id = aws_vpc.main-vpc.id
  count  = var.deploy_private_subnets ? 1 : 0

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw1[count.index].id
  }

  tags = {
    Name  = var.vpc_name
    Owner = var.owner
  }
}

# RT for NAT2
resource "aws_route_table" "private_2" {
  vpc_id = aws_vpc.main-vpc.id
  count  = var.deploy_private_subnets ? 1 : 0

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw2[count.index].id
  }

  tags = {
    Name  = var.vpc_name
    Owner = var.owner
  }
}
