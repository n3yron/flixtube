# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami
# Get AMI ID by name
data "aws_ami" "my_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["CDEDD AMI BASE TEMPLATE*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["421572644019"]
}
# Create EC2
resource "aws_instance" "web" {
  ami                    = data.aws_ami.my_image.id
  instance_type          = "m5.xlarge"
  key_name               = data.aws_key_pair.key_pair.key_name
  vpc_security_group_ids = [aws_security_group.default.id] # Set of strings is required
  subnet_id              = aws_subnet.public_subnets["public_subnet_1"].id
  count                  = var.node_count

  tags = {
    Owner = var.owner
    Dept  = var.dept_id
    Name  = "ec2_${random_id.server.hex}"
  }
}
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/key_pair
data "aws_key_pair" "key_pair" {
  key_name = "DevOp-All"
}

resource "random_id" "server" {
  #  keepers = {
  # Generate a new id each time we switch to a new AMI id
  #    ami_id = var.ami_id
  #  }

  byte_length = 8
}
# Create EBS volumes
#resource "aws_ebs_volume" "example" {
#  availability_zone = var.availability_zone
#  size              = 40
#}

#resource "aws_volume_attachment" "ebs_att" {
#  device_name = "/dev/sdh"
#  volume_id   = aws_ebs_volume.example.id
#  instance_id = aws_instance.example.id
#}
