resource "aws_security_group" "efs_access" {
  name        = "efs_access"
  description = "Allow efs access"
  vpc_id      = aws_vpc.main-vpc.id

  ingress {
    description = "EFS mount target"
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    cidr_blocks = [var.cidr]
  }
  tags = {
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
  }
}

resource "aws_efs_file_system" "myfilesystem" {
  encrypted = true

  tags = {
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
    Name     = "${var.my_cluster_name}efs"
  }
}

output "aws_efs_id" {
  value     = aws_efs_file_system.myfilesystem.id
  sensitive = false
}

resource "aws_efs_access_point" "test" {
  file_system_id = aws_efs_file_system.myfilesystem.id
  posix_user {
    gid            = 1000
    uid            = 1000
    secondary_gids = [0]
  }
  root_directory {
    path = "/directory"
  }
}

resource "aws_efs_mount_target" "efs_target_1" {
  file_system_id  = aws_efs_file_system.myfilesystem.id
  subnet_id       = aws_subnet.private_subnets["private_subnet_1"].id
  security_groups = [aws_security_group.efs_access.id]
}
resource "aws_efs_mount_target" "efs_target_2" {
  file_system_id  = aws_efs_file_system.myfilesystem.id
  subnet_id       = aws_subnet.private_subnets["private_subnet_2"].id
  security_groups = [aws_security_group.efs_access.id]
}

data "aws_iam_policy" "data_amazoneks_efs_csi_driver_policy" {
  name       = "AmazonEKS_EFS_CSI_Driver_Policy"
  depends_on = [aws_eks_cluster.eks_cluster]
}

locals {
  oidc = split("//", data.aws_eks_cluster.example.identity[0].oidc[0].issuer)[1]
}

resource "aws_iam_role" "oicd_role" {
  name = "AmazonEKS_EFS_CSI_DriverRole"

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Federated" : "arn:aws:iam::${var.account_aws}.:oidc-provider/${local.oidc}"
        },
        "Action" : "sts:AssumeRoleWithWebIdentity",
        "Condition" : {
          "StringEquals" : {
            "${local.oidc}:sub" : "system:serviceaccount:kube-system:efs-csi-controller-sa"
          }
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "amazoneks_efs_csi_driver_policy" {
  role       = aws_iam_role.oicd_role.name
  policy_arn = "arn:aws:iam::421572644019:policy/AmazonEKS_EFS_CSI_Driver_Policy"
}

data "aws_iam_role" "data_oicd_role" {
  name       = aws_iam_role.oicd_role.name
  depends_on = [aws_eks_cluster.eks_cluster]
}

# https://github.com/kubernetes-sigs/aws-efs-csi-driver
resource "helm_release" "aws-efs-csi-driver" {
  name      = "aws-efs-csi-driver"
  namespace = "kube-system"
  #  reuse_values = true
  force_update = "true"

  repository = "https://kubernetes-sigs.github.io/aws-efs-csi-driver/"
  chart      = "aws-efs-csi-driver"
  set {
    name  = "image.repository"
    value = "602401143452.dkr.ecr.${var.region}.amazonaws.com/eks/aws-efs-csi-driver"
  }
  set {
    name  = "controller.serviceAccount.create"
    value = "false"
  }
  set {
    name  = "controller.serviceAccount.name"
    value = "efs-csi-controller-sa"
  }
  depends_on = [aws_eks_cluster.eks_cluster, kubernetes_storage_class_v1.efs-sc, kubernetes_service_account_v1.efs-service-account]
}
# SA
resource "kubernetes_service_account_v1" "efs-service-account" {
  metadata {
    name      = "efs-csi-controller-sa"
    namespace = "kube-system"
    annotations = {
      "eks.amazonaws.com/role-arn" = data.aws_iam_role.data_oicd_role.arn
    }
    labels = {
      "app.kubernetes.io/name" = "aws-efs-csi-driver"
    }
  }
  depends_on = [aws_eks_cluster.eks_cluster]
}
# SC
# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class_v1
resource "kubernetes_storage_class_v1" "efs-sc" {
  metadata {
    name = "efs-sc"
  }
  storage_provisioner = "efs.csi.aws.com"
  reclaim_policy      = "Retain"
  parameters = {
    #    type             = "pd-standard"
    provisioningMode = "efs-ap"
    fileSystemId     = aws_efs_file_system.myfilesystem.id
    directoryPerms   = "755"
    gidRangeStart    = "1000"
    gidRangeEnd      = "2000"
    basePath         = "/dynamic_provisioning"
  }
  depends_on = [aws_eks_cluster.eks_cluster]
}
# PV
# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/persistent_volume
#resource "kubernetes_persistent_volume_v1" "efs-pv" {
#  metadata {
#    name = "efs-pv1"
#  }
#  spec {
#    storage_class_name = "efs-sc"
#    capacity = {
#      storage = "5Gi"
#    }
#    access_modes = ["ReadWriteMany"]
#    persistent_volume_source {
#      csi {
#        driver        = "efs.csi.aws.com"
#        volume_handle = aws_efs_file_system.myfilesystem.id
#      }
#    }
#  }
#  depends_on = [aws_eks_cluster.eks_cluster]
#}




### EFS SCI
# https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html
#resource "aws_iam_policy" "amazoneks_efs_csi_driver_policy" {
#  name        = "AmazonEKS_EFS_CSI_Driver_Policy"
#  path        = "/"
#  description = "AmazonEKS_EFS_CSI_Driver_Policy"
#
#  # Terraform's "jsonencode" function converts a
#  # Terraform expression result to valid JSON syntax.
#  policy = jsonencode({
#    "Version" : "2012-10-17",
#    "Statement" : [
#      {
#        "Effect" : "Allow",
#        "Action" : [
#          "elasticfilesystem:DescribeAccessPoints",
#          "elasticfilesystem:DescribeFileSystems",
#          "elasticfilesystem:DescribeMountTargets",
#          "ec2:DescribeAvailabilityZones"
#        ],
#        "Resource" : "*"
#      },
#      {
#        "Effect" : "Allow",
#        "Action" : [
#          "elasticfilesystem:CreateAccessPoint"
#        ],
#        "Resource" : "*",
#        "Condition" : {
#          "StringLike" : {
#            "aws:RequestTag/efs.csi.aws.com/cluster" : "true"
#          }
#        }
#      },
#      {
#        "Effect" : "Allow",
#        "Action" : "elasticfilesystem:DeleteAccessPoint",
#        "Resource" : "*",
#        "Condition" : {
#          "StringEquals" : {
#            "aws:ResourceTag/efs.csi.aws.com/cluster" : "true"
#          }
#        }
#      }
#    ]
#  })
#}

#resource "aws_efs_file_system_policy" "policy" {
#  file_system_id = aws_efs_file_system.myfilesystem.id#
#
#  bypass_policy_lockout_safety_check = true#
#
#  policy = <<POLICY
#{
#    "Version": "2012-10-17",
#    "Id": "ExamplePolicy01",
#    "Statement": [
#        {
#            "Sid": "ExampleStatement01",
#            "Effect": "Allow",
#            "Principal": {
#                "AWS": "*"
#            },
#            "Resource": "${aws_efs_file_system.myfilesystem.arn}",
#            "Action": [
#                "elasticfilesystem:ClientMount",
#                "elasticfilesystem:ClientWrite"
#            ],
#            "Condition": {
#                "Bool": {
#                    "aws:SecureTransport": "true"
#                }
#            }
#        }
#    ]
#}
#POLICY
#}
