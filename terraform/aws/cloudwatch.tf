resource "aws_cloudwatch_dashboard" "eks_resources_memory_dashboard" {
  dashboard_name = "eks_cluster_dashboard_n3yron"

  dashboard_body = <<EOF
{
    "widgets": [
        {
            "height": 6,
            "width": 6,
            "y": 0,
            "x": 0,
            "type": "metric",
            "properties": {
                "region": "us-east-2",
                "metrics": [
                    [ "ContainerInsights", "node_memory_utilization", "ClusterName", "eksasd-eks", { "stat": "Average" } ]
                ],
                "view": "timeSeries",
                "stacked": true,
                "start": "-PT3H",
                "end": "P0D",
                "period": 60,
                "annotations": {
                    "horizontal": [
                        {
                            "label": "node_memory_utilization >= 80 for 1 datapoints within 1 minute",
                            "value": 80
                        }
                    ]
                },
                "title": "Node memory usage with trashold"
            }
        },
        {
            "type": "metric",
            "x": 6,
            "y": 0,
            "width": 4,
            "height": 6,
            "properties": {
                "region": "us-east-2",
                "title": "CPU Utilization",
                "legend": {
                    "position": "bottom"
                },
                "timezone": "Local",
                "metrics": [
                    [ "ContainerInsights", "pod_cpu_utilization", "Namespace", "kube-system", "ClusterName", "eksasd-eks", { "stat": "Average" } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "stat": "Average" } ],
                    [ "...", "default", ".", ".", { "stat": "Average" } ]
                ],
                "liveData": false,
                "period": 60
            }
        },
        {
            "type": "metric",
            "x": 10,
            "y": 0,
            "width": 4,
            "height": 6,
            "properties": {
                "region": "us-east-2",
                "title": "Memory Utilization",
                "legend": {
                    "position": "bottom"
                },
                "timezone": "Local",
                "metrics": [
                    [ "ContainerInsights", "pod_memory_utilization", "Namespace", "kube-system", "ClusterName", "eksasd-eks", { "stat": "Average" } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "stat": "Average" } ],
                    [ "...", "default", ".", ".", { "stat": "Average" } ]
                ],
                "liveData": false,
                "period": 60
            }
        },
        {
            "type": "metric",
            "x": 14,
            "y": 0,
            "width": 4,
            "height": 6,
            "properties": {
                "region": "us-east-2",
                "title": "Network",
                "legend": {
                    "position": "right"
                },
                "timezone": "Local",
                "metrics": [
                    [ { "id": "expr0m0", "label": "kube-system pod_network_rx_bytes", "expression": "mm0m0 + mm0farm0", "stat": "Average", "yAxis": "left" } ],
                    [ { "id": "expr0m1", "label": "amazon-cloudwatch pod_network_rx_bytes", "expression": "mm0m1 + mm0farm1", "stat": "Average", "yAxis": "left" } ],
                    [ { "id": "expr0m2", "label": "default pod_network_rx_bytes", "expression": "mm0m2 + mm0farm2", "stat": "Average", "yAxis": "left" } ],
                    [ "ContainerInsights", "pod_network_rx_bytes", "Namespace", "kube-system", "ClusterName", "eksasd-eks", { "id": "mm0m0", "visible": false, "yAxis": "left" } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "id": "mm0m1", "visible": false, "yAxis": "left" } ],
                    [ "...", "default", ".", ".", { "id": "mm0m2", "visible": false, "yAxis": "left" } ],
                    [ ".", ".", "ClusterName", "eksasd-eks", "Namespace", "kube-system", "LaunchType", "fargate", { "id": "mm0farm0", "visible": false, "yAxis": "left" } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "id": "mm0farm1", "visible": false, "yAxis": "left" } ],
                    [ "...", "default", ".", ".", { "id": "mm0farm2", "visible": false, "yAxis": "left" } ],
                    [ { "id": "expr1m0", "label": "kube-system pod_network_tx_bytes", "expression": "mm1m0 + mm1farm0", "stat": "Average", "yAxis": "right" } ],
                    [ { "id": "expr1m1", "label": "amazon-cloudwatch pod_network_tx_bytes", "expression": "mm1m1 + mm1farm1", "stat": "Average", "yAxis": "right" } ],
                    [ { "id": "expr1m2", "label": "default pod_network_tx_bytes", "expression": "mm1m2 + mm1farm2", "stat": "Average", "yAxis": "right" } ],
                    [ "ContainerInsights", "pod_network_tx_bytes", "Namespace", "kube-system", "ClusterName", "eksasd-eks", { "id": "mm1m0", "visible": false, "yAxis": "right" } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "id": "mm1m1", "visible": false, "yAxis": "right" } ],
                    [ "...", "default", ".", ".", { "id": "mm1m2", "visible": false, "yAxis": "right" } ],
                    [ ".", ".", "ClusterName", "eksasd-eks", "Namespace", "kube-system", "LaunchType", "fargate", { "id": "mm1farm0", "visible": false, "yAxis": "right" } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "id": "mm1farm1", "visible": false, "yAxis": "right" } ],
                    [ "...", "default", ".", ".", { "id": "mm1farm2", "visible": false, "yAxis": "right" } ]
                ],
                "liveData": false,
                "period": 60
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 6,
            "width": 4,
            "height": 6,
            "properties": {
                "region": "us-east-2",
                "title": "Pod CPU Utilization",
                "legend": {
                    "position": "right"
                },
                "timezone": "Local",
                "metrics": [
                    [ { "id": "expr1m0", "label": "kube-system", "expression": "mm1m0 + mm1farm0" } ],
                    [ { "id": "expr1m1", "label": "amazon-cloudwatch", "expression": "mm1m1 + mm1farm1" } ],
                    [ { "id": "expr1m2", "label": "default", "expression": "mm1m2 + mm1farm2" } ],
                    [ "ContainerInsights", "pod_cpu_utilization_over_pod_limit", "Namespace", "kube-system", "ClusterName", "eksasd-eks", { "id": "mm1m0", "visible": false } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "id": "mm1m1", "visible": false } ],
                    [ "...", "default", ".", ".", { "id": "mm1m2", "visible": false } ],
                    [ ".", ".", "ClusterName", "eksasd-eks", "Namespace", "kube-system", "LaunchType", "fargate", { "id": "mm1farm0", "visible": false } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "id": "mm1farm1", "visible": false } ],
                    [ "...", "default", ".", ".", { "id": "mm1farm2", "visible": false } ]
                ],
                "liveData": false,
                "period": 60
            }
        },
        {
            "type": "metric",
            "x": 4,
            "y": 6,
            "width": 4,
            "height": 6,
            "properties": {
                "region": "us-east-2",
                "title": "Pod Memory Utilization",
                "legend": {
                    "position": "right"
                },
                "timezone": "Local",
                "metrics": [
                    [ { "id": "expr1m0", "label": "kube-system", "expression": "mm1m0 + mm1farm0" } ],
                    [ { "id": "expr1m1", "label": "amazon-cloudwatch", "expression": "mm1m1 + mm1farm1" } ],
                    [ { "id": "expr1m2", "label": "default", "expression": "mm1m2 + mm1farm2" } ],
                    [ "ContainerInsights", "pod_memory_utilization_over_pod_limit", "Namespace", "kube-system", "ClusterName", "eksasd-eks", { "id": "mm1m0", "visible": false } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "id": "mm1m1", "visible": false } ],
                    [ "...", "default", ".", ".", { "id": "mm1m2", "visible": false } ],
                    [ ".", ".", "ClusterName", "eksasd-eks", "Namespace", "kube-system", "LaunchType", "fargate", { "id": "mm1farm0", "visible": false } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "id": "mm1farm1", "visible": false } ],
                    [ "...", "default", ".", ".", { "id": "mm1farm2", "visible": false } ]
                ],
                "liveData": false,
                "period": 60
            }
        },
        {
            "type": "metric",
            "x": 8,
            "y": 6,
            "width": 4,
            "height": 6,
            "properties": {
                "region": "us-east-2",
                "title": "Number of Pods",
                "legend": {
                    "position": "bottom"
                },
                "timezone": "Local",
                "metrics": [
                    [ "ContainerInsights", "namespace_number_of_running_pods", "Namespace", "kube-system", "ClusterName", "eksasd-eks", { "stat": "Average" } ],
                    [ "...", "amazon-cloudwatch", ".", ".", { "stat": "Average" } ],
                    [ "...", "default", ".", ".", { "stat": "Average" } ]
                ],
                "liveData": false,
                "period": 60
            }
        }
    ]
}
EOF
}

#resource "aws_cloudwatch_metric_alarm" "foobar" {
#  alarm_name                = "terraform-test-foobar5"
#  comparison_operator       = "GreaterThanOrEqualToThreshold"
#  evaluation_periods        = "2"
#  metric_name               = "CPUUtilization"
#  namespace                 = "AWS/EC2"
#  period                    = "120"
#  statistic                 = "Average"
#  threshold                 = "80"
#  alarm_description         = "This metric monitors ec2 cpu utilization"
#  insufficient_data_actions = []
#}
