resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main-vpc.id

  tags = {
    Name  = var.vpc_name
    Owner = var.owner
  }
}
