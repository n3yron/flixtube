# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/nat_gateway
resource "aws_nat_gateway" "nat_gw1" {
  allocation_id = aws_eip.nat1[count.index].id
  subnet_id     = aws_subnet.public_subnets["public_subnet_1"].id
  count         = var.deploy_private_subnets ? 1 : 0
  tags = {
    Name  = "gw NAT"
    Owner = var.owner
  }
  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.gw]
}
resource "aws_nat_gateway" "nat_gw2" {
  allocation_id = aws_eip.nat2[count.index].id
  subnet_id     = aws_subnet.public_subnets_2["public_subnet_2"].id
  count         = var.deploy_private_subnets ? 1 : 0
  tags = {
    Name  = "gw NAT"
    Owner = var.owner
  }
  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.gw]
}
